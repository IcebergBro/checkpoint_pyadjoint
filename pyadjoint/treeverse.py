cps = []
count = 0

def get_kidstart(start, finis):
    # This method chooses the next starting point, implies
    # provides us the information where we should place 
    # the next check point

    if (start + finis) % 2 != 0:
        return int((start + finis)/(2)) + 1
    else:
        return int((start + finis)/2)

def forward_compute(n_free_cps, start, finis, my_blocks):
    global count
    for i in range(start):
        print("-", end = '')
    for i in range(start, finis):
        print("D", end = '')
    print('')
    put_checkpoint(start, my_blocks)
    put_checkpoint(finis-1, my_blocks)
    for i in range(start, finis):
        print("")
        print("recompute block " + str(i))
        print("")
        inputs = my_blocks[i].get_dependencies()
        for k, in_put in enumerate(inputs):
            print("before recompute, block " + str(i) + "'s " + str(k) + " dependency has check point " + str(in_put.checkpoint))
        my_blocks[i].recompute()
        
        #print('Recompute of block ', i, type(my_blocks[i]))
        #print(start, finis, i)
        for k, in_put in enumerate(inputs):
            print("after recompute:")
            if i > start and i < finis-n_free_cps-1:
                has_future_block = False
                for block_positions in in_put.dep_to_blocks_positions:
                    if block_positions > i:
                        has_future_block = True
                print("Input " + str(k) + " of block " + str(i) + " was produced at " + str(in_put.was_produced_at_block_position))
                '''if has_future_block:
                    print("It points to future block") 
                if not has_future_block:
                    print("It doesn't point to future block")'''
                if in_put.was_produced_at_block_position > start and not has_future_block:
                    in_put.checkpoint = None
                    print("I removed a check point!")
            print("Input " + str(k) + " of blcok " + str(i) + "'s check point is " + str(in_put.checkpoint))
            #        
        count += 1

def compute_adjoint(start, finis, my_blocks):
    #include the piesewiese broken tape to this method later
    while start < 0:
        start += 1
    for i in range(start):
        print("-", end = '')
    for i in range(start, finis):
        print("C", end = '')
    print('')
    for i in range(finis - 1, start - 1, -1):
        if i == len(my_blocks) - 1:
            my_blocks[i].get_outputs()[0].adj_value = 1.0
        elif my_blocks[i].get_outputs()[0].adj_value == None:
            print ("A broken piese of Block")
        #print('Calling ', type(my_blocks[i]))
        my_blocks[i].evaluate_adj()
        #print("computed adjoint for block " + str(i))

def put_checkpoint(position, my_blocks):
    to_get_checked_input = my_blocks[position].get_dependencies()
    for i in range(len(to_get_checked_input)):
        to_get_checked_input[i].should_save = True

    to_get_checked_output = my_blocks[position].get_outputs()
    for i in range(len(to_get_checked_output)):
        to_get_checked_output[i].should_save = True

def remove_checkpoint(position, my_blocks):
    to_get_checked_input = my_blocks[position].get_dependencies()
    for i in range(len(to_get_checked_input)):
        to_get_checked_input[i].should_save = False

    to_get_checked_output = my_blocks[position].get_outputs()
    for i in range(len(to_get_checked_output)):
        to_get_checked_output[i].should_save = False


def treeverse(n_free_cps, base, start, finis, my_blocks):
    # This is the main method to put check points given 
    # number of check points availble, initial position: base, 
    # starting posistion: start, and the length of sequential  
    # forward chain: finis of the list of blocks: my_blocks


    #print("I'm procrastinating to solve for interval (" + str(base) + "," + str(start) + ")!")
    #print("I'm not lazy! I am solving for internval (" + str(start) + "," + str(finis) + ")!")

    forward_compute(n_free_cps, base, start, my_blocks)
    
    global cps
    cps.append(start)
    print(cps)
    if start == 0:
        put_checkpoint(start, my_blocks)
    else:
        put_checkpoint(start - 1, my_blocks)

    while start < finis - n_free_cps and n_free_cps > 1:
        kidstart = get_kidstart(start, finis)
        #record_cps_forward.append(kidstart)
        
        if finis - n_free_cps < kidstart:
            kidstart = finis - n_free_cps + 1

        n_free_cps -= 1

        treeverse(n_free_cps, start, kidstart, finis, my_blocks)

        finis = kidstart - 1
        n_free_cps += 1
    
    
    if n_free_cps == 1:
        while (finis > start):
            cps.append(finis)
            put_checkpoint(finis - 1, my_blocks)
            n_free_cps -= 1
            should_save = True
            forward_compute(n_free_cps, start, finis, my_blocks)
            compute_adjoint(finis - 1 , finis, my_blocks)
            cps.pop()
            remove_checkpoint(finis - 1, my_blocks)
            n_free_cps += 1
            finis -= 1


        if start == cps[-1] and start > base:
            compute_adjoint(finis-1, finis, my_blocks)
            remove_checkpoint(finis - 1, my_blocks)
            finis = cps.pop()

    elif n_free_cps > 1:
        for i in range(n_free_cps):
            put_checkpoint(finis-i-1, my_blocks)
        forward_compute(n_free_cps, start, finis, my_blocks)
        compute_adjoint(start - 1, finis, my_blocks)
        for i in range(n_free_cps+1):
            remove_checkpoint(finis-i-2, my_blocks)

    return count
    