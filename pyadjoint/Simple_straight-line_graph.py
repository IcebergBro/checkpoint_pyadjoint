from fenics import *
from fenics_adjoint import *

tape.use_checkpoints(2)

mesh = IntervalMesh(10, 0, 1)
V = FunctionSpace(mesh, "CG", 1)

u_prev = Function(V)
u_curr = Function(V)
ic = interpolate(Expression('x[0]', degree=1), V)
u_prev.vector()[:] = ic.vector()
c = Control(u_prev)

u = TrialFunction(V)
v = TestFunction(V)

a = inner(u, v)*dx + inner(grad(u), grad(v))*dx
L = inner(1, v)*dx + u_prev*inner(u_prev, v)*dx

for i in range(3):
    solve(a == L, u_curr)
    u_prev.assign(u_curr)
    #print(u_prev.vector().array())
for i in range(len(tape.get_blocks())):
	print("tape.get.get.get_should_save is " + str(tape.get_blocks()[i].get_dependencies()[0].should_save))

J = assemble(u_prev**2*dx)

tape = get_working_tape()
#tape.get_first_forward_checkpoints()

#print("There are " + str(len(tape.get_blocks())) + " blocks")
#print(tape.get_first_forward_checkpoints())
dJdu = compute_gradient(J, c)
#exit()
Jhat = ReducedFunctional(J, c)
Jhat(ic)
h = Function(V)
h.vector()[:] = 1
taylor_test(Jhat, ic, h, dJdm=dJdu._ad_dot(h))
#tape.visualise()
